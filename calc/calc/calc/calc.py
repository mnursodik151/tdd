# -*- coding: utf-8 -*-
from functools import reduce
"""Main module."""

class Calc:
    def add(self, *args):
        return sum(args)

    def sub(self, a, b):
        return a-b

    def mul(self, *args):
        return reduce(lambda x, y: x*y, args)
