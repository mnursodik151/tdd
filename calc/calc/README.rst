====
calc
====


.. image:: https://img.shields.io/pypi/v/calc.svg
        :target: https://pypi.python.org/pypi/calc

.. image:: https://img.shields.io/travis/audreyr/calc.svg
        :target: https://travis-ci.org/audreyr/calc

.. image:: https://readthedocs.org/projects/calc/badge/?version=latest
        :target: https://calc.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




calc test for td


* Free software: MIT license
* Documentation: https://calc.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
