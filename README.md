# TDD

Hasil percobaan TDD

Untuk project calculator sebagai pengenalan terhadap TDD semua percobaan berjalan lancar dengan hasil sesuai yang diharapkan. Secara dasar workflow TDD adalah membuat rancangan testing terlebih dahulu baru kemudian membuat kode program yang dapat memenuhi kondisi darii test tersebut. Kendala pada bagian awal adalah mungkin ketidakcocokan saat membuat virtual environment sehingga saat melakukan test tidak langsung menggunakan py.test namun menggunakan python -m pytest. Namun hasil yang dicapai masih sesuai dengan yang ditunjukan dalam buku

Untuk project rentomatic sebgai lanjutan sekaligus sebagai pengenalan terhadap arsitektur sistem yang umum digunakan pada TDD. Percobaan berjalan lancar sampai dengan pembuatan web server dengan flask dan pembuatan request response. namun sekali lagi kendala dalam implementasi adalah saat melakukan test tidak dapat memakai py.test sehingga sampai sekarang belum bisa Untuk melakukan percobaan storage mmenggunakan postgre karena pada chapter tersebut terdapat langkah membuat test module sedangkan selama ini untuk melakukan pengujian sudah menggunakan module pytest
